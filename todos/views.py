from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm
# Create your views here.

def to_do_list(request):
    todos = TodoList.objects.all()
    context = {
        "to_do_list": todos,
    }
    return render(request, "todos/todos.html", context)

def to_do_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    tasks =  todolist.items.all()
    context = {
        "todolist": todolist,
        "tasks": tasks,
    }
    return render(request, "todos/detail.html", context)

def create_todolist(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save(False)
            todolist.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)


def edit_post(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm(instance=todolist)
    context = {
        "form": form,
        "todolist": todolist,
    }
    return render(request, "todos/edit.html", context)

def delete_list(request, id):
  model_instance = TodoList.objects.get(id=id)
  if request.method == "POST":
    model_instance.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html",)


def create_todoitems(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            to_doitem = form.save(commit=False)
            to_doitem.save()
            return redirect("todo_list_detail", id=to_doitem.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form
    }
    return render(request, "todos/itemscreate.html", context)


def edit_todoitems(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.list.id)
    else:
        form = TodoItemForm(instance=todoitem)
    context = {
        "form": form,
        "todoitem": todoitem,
    }
    return render(request, "todos/itemsedit.html", context)