from django.urls import path
from todos.views import to_do_list, to_do_list_detail, create_todolist, edit_post, delete_list, create_todoitems, edit_todoitems

urlpatterns = [
    path("", to_do_list, name="todo_list_list"),
    path("<int:id>/", to_do_list_detail, name="todo_list_detail"),
    path("create/", create_todolist, name="todo_list_create"),
    path("<int:id>/edit/", edit_post, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", create_todoitems, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_todoitems, name="todo_item_update"),

]
